#!/usr/bin/perl
#
# Copyright (c) 2014  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use strict;
use warnings FATAL => 'all';

use Cwd qw/abs_path/;
use File::Which qw/which/;
use File::Spec;
use Getopt::Std;
use POSIX qw/:sys_wait_h/;

my $debug = 0;
my $ldconfig_cache;
my %flavor = (os => 'unknown', dist => 'unknown');

sub debug($);
sub usage($);
sub version();

sub get_program_path($);
sub get_program_data($; $ $);
sub read_elf($ $);
sub compare_elf($ $);
sub extract_hashbang_program($ $ $);
sub check_wait_result($ $ $);
sub get_ldconfig_cache();
sub get_os_flavor();

MAIN:
{
	my %opts;

	getopts('e:htVv', \%opts) or usage(1);
	version() if $opts{V};
	usage(0) if $opts{h};
	exit(0) if $opts{V} || $opts{h};
	$debug = $opts{v};

	usage(1) unless $opts{e};
	my $progname = $opts{e};
	my $displayonly = $opts{t};

	usage(1) unless @ARGV || $displayonly;

	get_os_flavor();
	debug("Detected OS '$flavor{os}', distribution '$flavor{dist}'");

	my $progdata = get_program_data($progname);
	my $cache = get_ldconfig_cache();
	my @triplets = keys %{$cache->{libs}};
	my @dirs = @{$cache->{dirs}};

	if ($displayonly) {
		my @all;
		foreach my $triplet (@triplets) {
			next unless $triplet;
			debug("Checking a $triplet library...");
			my $libs = $cache->{'libs'}->{$triplet};
			my $libname = (keys %{$libs})[0];
			my $libfile = $libs->{$libname}->[0];
			my $res = read_elf($libname, $libfile);
			if (compare_elf($res, $progdata->{elf})) {
				debug("- match for $libfile!");
				push @all, $triplet;
			}
		}
		if (!@all) {
			die("No system libraries match $progname\n");
		}
		print join '', map "$_\n", @all;
		exit(0);
	}

	my @result;
	foreach my $name (@ARGV) {
		debug("Now checking for $name");
		my $match;
		foreach my $triplet (@triplets) {
			my $pathsref = $cache->{libs}{$triplet}{$name};
			next unless $pathsref;
			my @paths = @{$pathsref};
			debug("- found it for '$triplet' at @paths");
			foreach my $path (@paths) {
				my $res = read_elf($name, $path);
				if (compare_elf($res, $progdata->{elf})) {
					$match = $triplet;
					debug("- match!");
					last;
				}
			}
			last if defined($match);
		}
		if (defined($match)) {
			debug("- ok, we found a match for $name, ldconfig can handle it");
			push @result, $name;
			next;
		}

		debug("- now checking under @dirs");
		foreach my $dir (@dirs) {
			foreach my $triplet (@triplets) {
				my $fname = $triplet? "$dir/$triplet/$name": "$dir/$name";
				next unless -e $fname && -f $fname;
				debug("- found $fname");
				my $res = read_elf($name, $fname);
				if (compare_elf($res, $progdata->{elf})) {
					$match = $fname;
					debug("- match!");
					last;
				}
				last if defined($match);
			}
		}
		if (!defined($match)) {
			die("No match for $name\n");
		}
		push @result, $match;
	}

	print join(':', @result)."\n";
}

sub usage($)
{
	my ($err) = @_;
	my $s = <<'EOUSAGE';
Usage:	multiarch-find-libs [-tv] -e executable libname...
	multiarch-find-libs -V | -h

	-e	specify the executable file to load libraries for
	-h	display program usage information and exit
	-t	display the multiarch triplet(s) only
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE

	if ($err) {
		die($s);
	} else {
		print "$s";
	}
}

sub version()
{
	print "multiarch-find-libs 0.01\n";
}

sub debug($)
{
	print STDERR "RDBG $_[0]\n" if $debug;
}

sub check_wait_result($ $ $)
{
	my ($stat, $pid, $name) = @_;

	if (WIFEXITED($stat)) {
		if (WEXITSTATUS($stat) != 0) {
			die("Program '$name' (pid $pid) exited with non-zero status ".WEXITSTATUS($stat)."\n");
		}
	} elsif (WIFSIGNALED($stat)) {
		die("Program '$name' (pid $pid) was killed by signal ".WTERMSIG($stat)."\n");
	} elsif (WIFSTOPPED($stat)) {
		die("Program '$name' (pid $pid) was stopped by signal ".WSTOPSIG($stat)."\n");
	} else {
		die("Program '$name' (pid $pid) neither exited nor was killed or stopped; what does wait(2) status $stat mean?!\n");
	}
}

sub get_program_path($)
{
	my ($progname) = @_;

	# Is this more than a filename - a volume or a path specified?
	my ($volume, $path, $filename) = File::Spec->splitpath($progname);
	if (defined($volume) && $volume ne '' ||
	    defined($path) && $path ne '') {
		return undef unless -x $progname;
		return $progname;
	}

	# If it's just a filename, let's hope it's in the search path
	return which($progname);
}

sub extract_hashbang_program($ $ $)
{
	my ($f, $progname, $progpath) = @_;

	debug("extract_hashbang_program for $progname: $progpath");
	my $line = <$f>;
	close($f) or
	    die("Could not close '$progpath': $!\n");
	$line =~ s/[\r\n]*$//;
	debug("- about to examine $line");
	if ($line !~ /^\s*#!\s*(\S+)/) {
		return undef;
	}
	debug("- got $1");
	return $1;
}

sub get_ldconfig_cache()
{
	return $ldconfig_cache if defined $ldconfig_cache;

	if ($flavor{os} ne 'linux') {
		die("Don't know how to retrieve the loader's library cache for OS: $flavor{os}\n");
	}

	my $ldcfile;
	my $ldcpid = open($ldcfile, '-|');
	if (!defined($ldcpid)) {
		die("Could not fork for ldconfig: $!\n");
	} elsif ($ldcpid == 0) {
		my @cmd = ('ldconfig', '-p');

		debug("About to run @cmd");
		$ENV{LANG} = $ENV{LC_MESSAGES} = 'C';
		exec { $cmd[0] } @cmd;
		die("Could not run '@cmd': $!\n");
	}

	my $re_name = qr{[^\s(]+};
	my $re_comment = qr{[(].*[)]\s+};
	my $re_triplet = qr{[^\s/-]+-[^\s/-]+-[^\s/]+};
	my $re_path_w_triplet = qr{(\S*/lib)/($re_triplet)/\S+};
	my $re_path_wo_triplet = qr{\S+};
	my $re_w_triplet = qr{^\s*($re_name)\s*(?:$re_comment)?=>\s*($re_path_w_triplet)\s*$};
	my $re_wo_triplet = qr{^\s*($re_name)\s*(?:$re_comment)?=>\s*($re_path_wo_triplet)\s*$};

	my (%cache, %dirs);
	while (<$ldcfile>) {
		s/[\r\n]*$//;
		my ($libname, $libtriplet, $libdir, $libpath);
		if ($_ =~ $re_w_triplet) {
			($libname, $libpath, $libdir, $libtriplet) = ($1, $2, $3, $4);
			$dirs{$libdir} = 1;
		} elsif ($_ =~ $re_wo_triplet) {
			($libname, $libpath, $libtriplet) = ($1, $2, '');
		} else {
			next;
		}
		push @{$cache{$libtriplet}{$libname}}, $libpath;
	}

	$ldconfig_cache = { 'libs' => \%cache, 'dirs' => [ keys %dirs ] };
	return $ldconfig_cache;
}

sub get_os_flavor()
{
	if ($^O ne 'linux') {
		warn("multiarch-query only tested under Linux so far, expect problems\n");
		$flavor{os} = 'unknown';
		return;
	}
	$flavor{os} = 'linux';
	if (! -f '/etc/debian_version') {
		warn("multiarch-query only tested under Debian GNU/Linux so far, problems possible\n");
		$flavor{dist} = 'unknown';
		return;
	}
	$flavor{dist} = 'debian';
}

sub get_program_data($; $ $)
{
	my ($progname, $origname, $checked) = @_;

	$origname = $progname unless defined($origname);
	if (!defined($checked)) {
		$checked = {
			$progname => 1,
			$origname => 1,
			'' => [ $origname, ],
		};
		push @{$checked->{''}}, $progname if $progname ne $origname;
	}
	my $down = $origname ne $progname;
	
	debug("get_program_data for progname $progname, origname $origname, down ".($down? "true": "false"));
	my $progpath = get_program_path($progname);
	die("Could not find an executable for $progname ".($down? "(while looking for $origname) ": "")."\n") unless defined($progpath);
	debug("- got progpath $progpath");

	my $f;
	open($f, '<', $progpath) or
	    die("Could not open '$progpath' for reading: $!\n");
	my $four;
	if (sysread($f, $four, 4) < 4) {
		close($f);
		die("Could not even read three bytes from $progpath: $!\n");
	}
	debug("- first three bytes: ".unpack("H*", $four));
	if (substr($four, 0, 2) eq '#!') {
		# Pfth!
		debug("- a hashbang thing, let's see what the real interpreter is");
		sysseek($f, 0, 0) or die("Could not (sys)seek back to the start of $progpath: $!\n");
		seek($f, 0, 0) or die("Could not seek back to the start of $progpath: $!\n");
		my $hb = extract_hashbang_program($f, $progname, $progpath);
		close($f);
		return undef unless defined($hb);
		debug("- tail-recursing into get_program_data($hb)");
		if (exists($checked->{$hb})) {
			die("Hashbang interpreter loop for $origname: ".
			    join(', ', map { "'$_'" } @{$checked->{''}}).", '$hb'\n");
		}
		$checked->{$hb} = 1;
		push @{$checked->{''}}, $hb;
		return get_program_data($hb, $origname, $checked);
	} elsif (unpack("H*", $four) ne '7f454c46') {
		close($f);
		die("Not an ELF executable: $progpath\n");
	}
	close($f);

	return {
		'name' => $origname,
		'bin' => $progpath,
		'elf' => read_elf($progname, $progpath),
	};
}

sub read_elf($ $)
{
	my ($progname, $progpath) = @_;

	my $readelf;
	my $readelfpid = open($readelf, '-|');
	if (!defined($readelfpid)) {
		die("Could not fork for readelf: $!\n");
	} elsif ($readelfpid == 0) {
		my @cmd = ('eu-readelf', '-h', $progpath);
		debug("About to run @cmd");
		exec { $cmd[0] } @cmd;
		die("Could not run '@cmd': $!\n");
	}
	my @output = <$readelf>;
	close($readelf) or
	    die("Could not close the readelf pipe (pid $readelfpid): $!\n");
	my $readelfstat = $?;
	check_wait_result($readelfstat, $readelfpid, 'readelf');
	if (!@output) {
		die("No lines returned by readelf for $progname\n");
	}

	my @fields = qw{Class OS/ABI Machine};
	my $re = qr/\Q$fields[0]\E/;
	for (@fields[1..$#fields]) {
		$re = qr/$re|\Q$_\E/;
	}

	my %res = ();
	foreach (@output) {
		s/[\r\n]*$//;
		next unless m{^\s*($re):\s*(.*)$};
		my ($field, $value) = ($1, $2);
		$value =~ s/\s*$//;
		if (defined($res{$field})) {
			die("readelf returned duplicate '$field' for $progpath\n");
		}
		$res{$field} = $value;
	}
	my @missing = grep !defined($res{$_}), @fields;
	die("readelf did not return @missing for $progpath\n") if @missing;
	debug("- got ".join(' ', map { "$_: '$res{$_}'" } @fields));
	return \%res;
}

sub compare_elf($ $)
{
	my ($e1, $e2) = @_;
	return unless
	    defined(ref($e1)) && ref($e1) eq 'HASH' &&
	    defined(ref($e2)) && ref($e2) eq 'HASH';

	my @k = keys %{$e1};
	return unless scalar(@k) == scalar(keys(%{$e2}));

	foreach (@k) {
		return unless exists($e2->{$_}) && $e2->{$_} eq $e1->{$_};
	}
	return 1;
}
